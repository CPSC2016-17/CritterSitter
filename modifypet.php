<?php
include_once("Scripts/dbconnect.php");

session_start();

if(isset($_POST['submit-modify']))
{
	editPet();
}

function editPet()
{
	global $connection;
	
	if($_SESSION['type'] == "ind")
	{
		$updatePet = "UPDATE pet SET 
							name='". $_POST['name'] ."',
							pic='". $_POST['pic'] ."', 
							colour='". $_POST['colour'] ."', 
							b_date='". $_POST['b_date'] ."', 
							sex='". $_POST['sex'] ."', 
							species='". $_POST['species'] ."', 
							friendliness='". $_POST['friendliness'] ."' 
					  WHERE id='". $_POST['id_of_pet'] ."';";
		$updateOwns = "UPDATE owns SET
							adoption_date='". $_POST['adoption_date'] ."'
					  WHERE pet_id='". $_POST['id_of_pet'] ."' 
					  		AND user='". $_SESSION['user'] ."';";
					  
		$connection->query($updatePet);
		$connection->query($updateOwns);
		
		header("Location: mypets.php");
	}
	
	if($_SESSION['type'] == "org")
	{
		$updatePet = "UPDATE pet SET 
							name='". $_POST['name'] ."',
							pic='". $_POST['pic'] ."', 
							colour='". $_POST['colour'] ."', 
							b_date='". $_POST['b_date'] ."', 
							sex='". $_POST['sex'] ."', 
							species='". $_POST['species'] ."', 
							friendliness='". $_POST['friendliness'] ."' 
					  WHERE id='". $_POST['id_of_pet'] ."'";
		$updateOwns = "UPDATE cares_for SET
							source='". $_POST['source'] ."'
					  WHERE pet_id='". $_POST['id_of_pet'] ."' 
					  		AND org='". $_SESSION['user'] ."';";
					  
		$connection->query($updatePet);
		$connection->query($updateOwns);
		
		header("Location: mypets.php");
	}
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - My Pets</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/addpet.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	
    <h2>Modify Pet</h2>
	<hr />
    
    <div id="inputform">
    	<form name="editPet" action="" method="post">
        	<table cellpadding="5px">
            <tbody>
            
            <?php 
				
				$sql;
		
				if($_SESSION['type'] == "ind")  
				{
					$sql = "SELECT name, pic, colour, b_date, sex, species, friendliness, adoption_date FROM pet, owns WHERE id=pet_id AND id = '". $_GET['id'] ."' AND user = '".$_SESSION['user']."'; ";
				}
				
				if($_SESSION['type'] == "org")
				{
					$sql = "SELECT name, pic, colour, b_date, sex, species, friendliness, source FROM pet, cares_for WHERE id=pet_id AND id = '". $_GET['id'] ."' AND org = '".$_SESSION['user']."'; ";
				}
				
				$result = $connection->query($sql);
				
				$pet = mysqli_fetch_assoc($result);			
			?>
            
 				<tr><td><label>Name:</label></td><td><input type="text" required="required" name="name"value="<?php echo $pet['name']?>" /></td></tr>
                <tr><td><label>Picture Url:</label></td><td><input type="text" required="required" name="pic" value="<?php echo $pet['pic']?>" /></td></tr>
                <tr><td><label>Colour:</label></td><td style="text-align:left"><select name="colour">
                						<option <?php if($pet['colour'] == 'Black') echo ' selected ' ?> value="Black">Black</option>
                                        <option <?php if($pet['colour'] == 'Brown') echo ' selected ' ?> value="Brown">Brown</option>
                                        <option <?php if($pet['colour'] == 'Red') echo ' selected ' ?> value="Red">Red</option>
                                        <option <?php if($pet['colour'] == 'White') echo ' selected ' ?> value="White">White</option>
                                        <option <?php if($pet['colour'] == 'Grey') echo ' selected ' ?> value="Grey">Grey</option>
                                        <option <?php if($pet['colour'] == 'Orange') echo ' selected ' ?> value="Orange">Orange</option>
                                        <option <?php if($pet['colour'] == 'Blue') echo ' selected ' ?> value="Blue">Blue</option>
                                        <option <?php if($pet['colour'] == 'Green') echo ' selected ' ?> value="Green">Green</option>
                                        <option <?php if($pet['colour'] == 'Spotted') echo ' selected ' ?> value="Spotted">Spotted</option>
                                        <option <?php if($pet['colour'] == 'Striped') echo ' selected ' ?> value="Striped">Striped</option>
                                        <option <?php if($pet['colour'] == 'Other') echo ' selected ' ?> value="Other">Other</option>
                                      </select></td></tr>
                <tr><td><label>Birthday:</label></td><td style="text-align:left"><input type="date" required="required" name="b_date" value="<?php echo $pet['b_date']?>" /></td></tr>
                <tr><td><label>Sex:</label></td><td style="text-align:left"><select name="sex">
                						<option <?php if($pet['sex'] == 'M') echo ' selected ' ?> value="M">Male</option>
                                        <option <?php if($pet['sex'] == 'F') echo ' selected ' ?>value="F">Female</option>
                					</select></td></tr>
                <tr><td><label>Species:</label></td><td style="text-align:left"><select name="species">
                							<option <?php if($pet['species'] == 'Dog') echo ' selected ' ?> value="Dog" selected="selected">Dog</option>
                                            <option <?php if($pet['species'] == 'Cat') echo ' selected ' ?> value="Cat">Cat</option>
                                            <option <?php if($pet['species'] == 'Snake') echo ' selected ' ?> value="Snake">Snake</option>
                                            <option <?php if($pet['species'] == 'Lizard') echo ' selected ' ?> value="Lizard">Lizard</option>
                                            <option <?php if($pet['species'] == 'Fish') echo ' selected ' ?> value="Fish">Fish</option>
                                            <option <?php if($pet['species'] == 'Rabbit') echo ' selected ' ?> value="Rabbit">Rabbit</option>
                                            <option <?php if($pet['species'] == 'Rodent') echo ' selected ' ?> value="Rodent">Rodent</option>
                                            <option <?php if($pet['species'] == 'Frog') echo ' selected ' ?> value="Frog">Frog</option>
                                            <option <?php if($pet['species'] == 'Turtle') echo ' selected ' ?> value="Turtle">Turtle</option>
                                            <option <?php if($pet['species'] == 'Other') echo ' selected ' ?> value="Other">Other</option>
                						</select></td></tr>
                <tr><td><label>Friendliness:</label></td><td style="text-align:left"><input type="number" max="5" min="1" value="<?php echo $pet['friendliness']?>" required="required" name="friendliness" /></td></tr>
                
                <?php
					
					if($_SESSION['type'] == "ind")
					{
						echo '<tr><td><label>Adoption Date:</label></td><td style="text-align:left"><input type="date" required="required" name="adoption_date" value="'. $pet['adoption_date'] .'" /></td></tr>';
					}
					else
					{
						echo '<tr><td><label>Source:</label></td><td style="text-align:left"><select name="source">
								<option value="Street"'. ($pet['source'] == 'Street' ? ' selected ' : '') .'>Street</option>
								<option value="Abuse"'. ($pet['source'] == 'Abuse' ? ' selected ' : '') .'>Abuse</option>
								<option value="Unwanted"'. ($pet['source'] == 'Unwanted' ? ' selected ' : '') .'>Unwanted</option>
								<option value="Other"'. ($pet['source'] == 'Other' ? ' selected ' : '') .'>Other</option>
							  </select></td></tr>';
					}
				?>
                
            </tbody>
            </table>
            <br />
            <input type="hidden" name="id_of_pet" value="<?php echo $_GET['id'] ?>"/>
            <input type="submit" name="submit-modify" value="Save Changes" /> <input type="button" onclick="location.href='mypets.php'" value="Discard"/>
        </form>
    </div>
    
</div>
</body>
</html>
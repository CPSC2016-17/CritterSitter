<?php

include_once("Scripts/dbconnect.php");

session_start();
if(isset($_GET['logout']))
{
	session_destroy();
	header("Location: index.php");
}

if(isset($_POST['interested']))
{
	$insertInterested = "INSERT INTO interested_in VALUES('". $_SESSION['user'] ."', '". $_POST['int_ad_id'] ."');";
	$connection->query($insertInterested);
}

if(isset($_POST['not_interested']))
{
	$removeInterested = "DELETE FROM interested_in WHERE user='". $_SESSION['user'] ."' AND ad_id='". $_POST['int_ad_id'] ."';";
	$connection->query($removeInterested);
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/modal.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/Advertisements.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php if(!isset($_SESSION['user'])){include_once("loginmodal.php"); loadLoginModal();} ?>
<?php include_once("Scripts/header.php"); loadHeader(true); ?>



<div id = "content">
<?php 
		
	$canTakeInterest = false;	
		
	if($_SESSION['type'] == "ind")
	{	
		include_once("browse.php");
		
		$canTakeInterest = true;
		//SQL string is populated from browse.php
	}
	else if($_SESSION['type'] == "org")
	{
		//ORG, so only show this org's ads
		$sql = "SELECT pet.ad_id, ad.user, description, care_level, start_date, end_date, org_name AS poster, for_profit, id, name, pic, colour, b_date, sex, species, friendliness FROM `advertisment` AS ad, `pet`, `organization` AS org WHERE ad.user=org.user AND ad.user='". $_SESSION['user'] ."' AND pet.ad_id=ad.ad_id;";
	}
	else
	{
		//NOT LOGGED IN SO ONLY SHOW ORG. ADS
		$sql = "SELECT pet.ad_id, ad.user, description, care_level, start_date, end_date, org_name AS poster, for_profit, id, name, pic, colour, b_date, sex, species, friendliness FROM `advertisment` AS ad, `pet`, `organization` AS org WHERE ad.user=org.user AND pet.ad_id=ad.ad_id;";
	}
	
	$ads = $connection->query($sql);
	
	echo '<table class = "adTable"  >';
	
	while($row = mysqli_fetch_assoc($ads)){
		$ratingSql = "SELECT AVG(rating) AS avg_rating FROM feedback WHERE pet_id='" . $row['id'] . "';";
		
		$rating = $connection->query($ratingSql);
		$rating = mysqli_fetch_assoc($rating);
		$rating = round($rating['avg_rating'], 1);
		
		if($rating <= 0)
		{
			$rating = "N/A";
		}
		echo '<tr adRow>';	
			echo '<td class = "adCol1"> 
					<h3>'.	$row['name'] .'</h3>
					<img src= "'. $row['pic'] .'">
					<p>Rating: '. $rating .'</p>';
					if( $canTakeInterest and $row['user'] != $_SESSION['user'])
					{
						$isInterested = "SELECT ad_id FROM interested_in WHERE ad_id='". $row['ad_id'] . "';";
						
						$isInt = $connection->query($isInterested);
						
						if($isInt->num_rows > 0)
						{
							echo '<p><form action="" method="post"><input type="hidden" name="int_ad_id" value="'. $row['ad_id'] .'"/><input class="notIntButton" type="submit" name="not_interested" value="Interested"/></form></p>';
						}
						else
						{
							echo '<p><form action="" method="post"><input type="hidden" name="int_ad_id" value="'. $row['ad_id'] .'"/><input class="intButton" type="submit" name="interested" value="Take Interest"/></form></p>';			
						}
					}
			echo '</td>';
			echo 	'<td class = "adCol2"> 
						<p>Start Date: '.$row['start_date'] . '</p>
						<p>End Date: '.$row['end_date'] . '</p>
						<p>Care Needed: '.$row['care_level'] . '</p>
						<p>Born: '. $row['b_date'] .'</p>
						<p>Gender: '. $row['sex'] .'</p>
						<p>Colour: '. $row['colour'] .'</p>
						<p>Species: '. $row['species'] .'</p>
						<p>Friendliness: '. $row['friendliness'] .'</p>';
						
						$getPoster = "SELECT org_name, for_profit FROM organization WHERE user='". $row['user']. "';";
						$poster = $connection->query($getPoster);
						$poster = mysqli_fetch_assoc($poster);
						if(!empty($poster['org_name']))
						{
							 echo '<p>Posted by: ' . $poster['org_name'] .'</p>';
							 echo '<p>For Profit: '; echo ($poster['for_profit']) ? 'Yes' : 'No'; echo '</p>'; 
						}
				echo '</td>';
					
			
			echo 	'<td class = "adCol3"> <p>Equipment</p>
					<table class = "equipTable" border = 1> 
						<tr><th>Type</th><th>Description</th></tr>';
						$sql = "SELECT * FROM `equipment` WHERE  ad_id = '" . $row['ad_id'] . "';";
						$equip = $connection->query($sql);
											
						while($erow = mysqli_fetch_assoc($equip)){
							echo '<tr><td>';
							echo $erow['type'];
							echo '</td><td>';
							echo $erow['details'];
							echo '</td></tr>';
						}
			echo	'</table>
					</td>';
			echo 	'<td class = "adCol4">
					<p>Description</p>';
					echo $row['description'];
			echo 	'</td>';
			
		echo '</tr>';
		
	}
	
	
	
	echo "</table>";
	
?>
</div>

</body>
</html>
<?php
include_once("Scripts/dbconnect.php");

session_start();

if(isset($_POST['submit-addpet']))
{
	addPet();
}

function addPet()
{
	global $connection;
	
	if($_SESSION['type'] == "ind")
	{
		$insertPet = "INSERT INTO pet VALUES(null, '". $connection->real_escape_string($_POST['name']) . "', '". $_POST['pic'] . "', '". $_POST['colour'] . "', '". $_POST['b_date'] . "', '". $_POST['sex'] . "', '". $_POST['species'] . "', '". $_POST['friendliness'] . "', null);";
		$insertOwns = "INSERT INTO owns VALUES('". $_SESSION['user'] ."', LAST_INSERT_ID(), '". $_POST['adoption_date'] ."');"; 
		
		$result = $connection->query($insertPet);
		
		if($result)
		{
			$result = $connection->query($insertOwns);
			
			if($result)
			{
				header("Location: mypets.php");
			}
		}
	}
	
	if($_SESSION['type'] == "org")
	{
		$insertPet = "INSERT INTO pet VALUES(null, '". $_POST['name'] . "', '". $_POST['pic'] . "', '". $_POST['colour'] . "', '". $_POST['b_date'] . "', '". $_POST['sex'] . "', '". $_POST['species'] . "', '". $_POST['friendliness'] . "', null);";
		$insertOwns = "INSERT INTO cares_for VALUES('". $_SESSION['user'] ."', LAST_INSERT_ID(), '". $_POST['source'] ."');"; 
		
		$result = $connection->query($insertPet);
		
		if($result)
		{
			$result = $connection->query($insertOwns);
			
			if($result)
			{
				header("Location: mypets.php");
			}
		}
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - My Pets</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/addpet.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	
    <h2>New Pet</h2>
	<hr />
    
    <div id="inputform">
    	<form name="newPet" action="" method="post">
        	<table cellpadding="5px">
            <tbody>
 				<tr><td><label>Name:</label></td><td><input type="text" required="required" name="name" /></td></tr>
                <tr><td><label>Picture Url:</label></td><td><input type="text" required="required" name="pic" /></td></tr>
                <tr><td><label>Colour:</label></td><td style="text-align:left"><select name="colour">
                						<option selected="selected" value="Black">Black</option>
                                        <option value="Brown">Brown</option>
                                        <option value="Red">Red</option>
                                        <option value="White">White</option>
                                        <option value="Grey">Grey</option>
                                        <option value="Orange">Orange</option>
                                        <option value="Blue">Blue</option>
                                        <option value="Green">Green</option>
                                        <option value="Spotted">Spotted</option>
                                        <option value="Striped">Striped</option>
                                        <option value="Other">Other</option>
                                      </select></td></tr>
                <tr><td><label>Birthday:</label></td><td style="text-align:left"><input type="date" required="required" name="b_date" /></td></tr>
                <tr><td><label>Sex:</label></td><td style="text-align:left"><select name="sex">
                						<option value="M" selected="selected">Male</option>
                                        <option value="F">Female</option>
                					</select></td></tr>
                <tr><td><label>Species:</label></td><td style="text-align:left"><select name="species">
                							<option value="Dog" selected="selected">Dog</option>
                                            <option value="Cat">Cat</option>
                                            <option value="Snake">Snake</option>
                                            <option value="Lizard">Lizard</option>
                                            <option value="Fish">Fish</option>
                                            <option value="Rabbit">Rabbit</option>
                                            <option value="Rodent">Rodent</option>
                                            <option value="Frog">Frog</option>
                                            <option value="Turtle">Turtle</option>
                                            <option value="Other">Other</option>
                						</select></td></tr>
                <tr><td><label>Friendliness:</label></td><td style="text-align:left"><input type="number" max="5" min="1" value="3" required="required" name="friendliness" /></td></tr>
                
                <?php
					
					if($_SESSION['type'] == "ind")
					{
						echo '<tr><td><label>Adoption Date:</label></td><td style="text-align:left"><input type="date" required="required" name="adoption_date" /></td></tr>';
					}
					else
					{
						echo '<tr><td><label>Source:</label></td><td style="text-align:left"><select name="source"><option value="Street" selected="selected">Street</option><option value="Abuse">Abuse</option><option value="Unwanted">Unwanted</option><option value="Other">Other</option></select></td></tr>';
					}
				?>
                
            </tbody>
            </table>
            <br />
            <input type="submit" name="submit-addpet" value="Add" /> 
        </form>
    </div>
    
</div>
</body>
</html>
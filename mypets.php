<?php
include_once("Scripts/dbconnect.php");

session_start();

if(isset($_SESSION['sitter_found']))
{
	echo '<script type="text/javascript">';
	echo 'window.open("mailto: '. $_SESSION['sitter_found'] .'?subject=[Critter Sitter] Offer Accepted!");';
	echo '</script>';
	
	$_SESSION['sitter_found'] = NULL;
	
}

if(isset($_POST['remove_pet']))
{
	$removeAnyAds = "DELETE FROM advertisment WHERE ad_id=(SELECT ad_id FROM pet WHERE id='".$_POST['id_of_pet']."');";
	$removePet = "DELETE FROM pet WHERE id='" .$_POST['id_of_pet'] . "';";
	$connection->query($removeAnyAds);
	$connection->query($removePet);
	
	header("Refresh:0");
}

if(isset($_GET['del_ad']))
{
	$removeAd = "DELETE FROM advertisment WHERE ad_id='".$_GET['del_ad']."' AND user='". $_SESSION['user'] ."';";
	
	$connection->query($removeAd);
	
	header("Location: mypets.php");
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - My Pets</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/petresults.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	<h2>My Pets</h2>
    <hr />
    
    <table id="petTable" width="80%" border="1" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF">
    <th width="20%">Picture</th><th width="60%">Details</th><th width="20%">Actions</th>
    <?php
		$sql;
		
		if($_SESSION['type'] == "ind")  
		{
			$sql = "SELECT id, name, pic, colour, b_date, sex, species, friendliness, ad_id, adoption_date FROM pet, owns WHERE id=pet_id AND user = '".$_SESSION['user']."'; ";
		}
		
		if($_SESSION['type'] == "org")
		{
			$sql = "SELECT id, name, pic, colour, b_date, sex, species, friendliness, ad_id, source FROM pet, cares_for WHERE id=pet_id AND org = '".$_SESSION['user']."'; ";
		}
		
		$result = $connection->query($sql);
		
		while($row = mysqli_fetch_assoc($result))
		{
			
			$ratingSql = "SELECT AVG(rating) AS avg_rating FROM feedback WHERE pet_id='" . $row['id'] . "';";
			
			$rating = $connection->query($ratingSql);
			$rating = mysqli_fetch_assoc($rating);
			$rating = round($rating['avg_rating'], 1);
				
			if($rating <= 0)
			{
				$rating = "N/A";
			}
			
			echo '<tr>';
				echo '<td width="20%"><img width="100%" height="auto" src="'. $row['pic'] .'"></td>';
				echo '<td width="60%">';
					echo '<div class="petinfo">';
					echo '<h3>'. $row['name'] .'</h3><p>Average Rating:' . $rating . '<p>';
					echo '<hr />';
					echo '<table width="100%">';
						echo '<tr>';
						echo '<td>Born on: ' . $row['b_date'] . '</td>';
						
						if($_SESSION['type'] == "ind")
						{
							echo '<td>Adopted on: ' . $row['adoption_date'] . '</td>';	
						}
						else
						{
							echo '<td>Source: ' . $row['source'] . '</td>';
						}
						
						echo '<td>Species: ' . $row['species'] . '</td>';
						echo '</tr>';
						echo '<tr>';
						echo '<td>Gender: ' . $row['sex'] . '</td>';
						echo '<td>Colour: ' . $row['colour'] . '</td>';
						echo '<td>Friendliness: ' . $row['friendliness'] . '</td>';
						echo '</tr>';
					echo '</table>';
					echo '</div>';
				echo '</td>';
				echo '<td width="20%">';
					echo '<form method="post">';
					echo '<input type="hidden" name="id_of_pet" value="'. $row['id'] .'"/>';
					echo '<input type="button" onclick="location.href=\'viewfeedback.php?id='. $row['id'] .'\'" name="view_feedback" value="View Feedback"/><br/>';
					
					if(empty($row['ad_id']))	//No Ad Currently running for pet
					{
						echo '<input type="button" onclick="location.href=\'postad.php?id='. $row['id'] .'\'" name="post_ad" value="Create Ad"/><br/>';	
					}
					else	//Pet has an ad
					{
						echo '<input type="button" onclick="location.href=\'interestedsitters.php?ad_id='. $row['ad_id'] .'\'" name="interested_sitters" value="Interested Sitters"/>';
						echo '<input type="button" onclick="location.href=\'mypets.php?del_ad='. $row['ad_id'] .'\'" name="delete_ad" value="Delete Ad"/><br/>';
					}
					
					
					echo '<input type="button" onclick="location.href=\'modifypet.php?id='. $row['id'] .'\'" name="edit_pet" value="Modify">';
					echo '<input type="submit" name="remove_pet" value="Remove"/>';
					echo '</form>';
				echo '</td>';
			echo '</tr>';
		}	
	?>
	<tr><td colspan="3"><button onclick="location.href='addpet.php'">Add Pet</button></td></tr>
	
    </table>
    
    
</div>

</body>
</html>
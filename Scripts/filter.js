// JavaScript Document

function openDropdown( id, label)
{
	var dropdown = document.getElementById(id);
	var header = document.getElementById(label);
	
	if(getComputedStyle(dropdown, null).display === "none")
	{
		dropdown.style.display = "block";
		header.innerHTML = header.innerHTML.substring(0, header.innerHTML.length - 1) + "▼";
	}
	else
	{
		dropdown.style.display = "none";
		header.innerHTML = header.innerHTML.substring(0, header.innerHTML.length - 1) + "◄";
	}
}
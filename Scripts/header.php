<?php

session_start();

function loadHeader($signup = false)
{
	echo '<h1 id="HeaderTitle"><a href="index.php">Critter Sitter</a></h1>';
	echo '<div id="menu">';
	echo '<ul>';
	
	if(isset($_SESSION['user']))
	{
		if($_SESSION['type'] == "ind")
		{
			echo '<li><a href="index.php" onmouseover="this.style.cursor=\'pointer\'">Browse Pets</a></li>';
		}
		else
		{
			echo '<li><a href="index.php" onmouseover="this.style.cursor=\'pointer\'">My Organization Ads</a></li>';
		}
		echo '<li><a href="myreviews.php" onmouseover="this.style.cursor=\'pointer\'">Feedback & Reviews</a></li>';
		echo '<li><a href="mypets.php" onmouseover="this.style.cursor=\'pointer\'">My Pets</a></li>';
		echo '<li><a href="index.php?logout=true" onmouseover="this.style.cursor=\'pointer\'">Logout</a></li>';
	}
	else
	{
		echo '<li><a href="index.php" onmouseover="this.style.cursor=\'pointer\'">Browse Pets</a></li>';
		echo '<li><a onClick="showLoginModal()" onmouseover="this.style.cursor=\'pointer\'">Login'; if($signup) {echo'/Signup';} echo'</a></li>';
	}
	
	
	echo '</ul>';
	echo '</div>';

}
?>
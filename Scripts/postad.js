
var id = 0;

function addEquipment()
{
	var table = document.getElementById("equipment_table");
	
	var index = table.rows.length;
	
	var row = table.insertRow(index);
	
	var typeRow = row.insertCell(0);
	var detailsRow = row.insertCell(1);
	
	typeRow.innerHTML = "<select name='type_" + id +"'><option selected>Tank</option><option>Cage</option><option>Leash</option><option>Food</option><option>Toy</option><option>Other</option></select>";
	detailsRow.innerHTML = "<textarea width='100%' name='details_" + id +"'></textarea>";
	
	id = id + 1;
	
}
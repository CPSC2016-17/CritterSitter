﻿# Host: localhost  (Version 5.7.17-log)
# Date: 2017-04-06 15:11:09
# Generator: MySQL-Front 6.0  (Build 1.86)


#
# Structure for table "advertisment"
#

DROP TABLE IF EXISTS `advertisment`;
CREATE TABLE `advertisment` (
  `ad_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL DEFAULT '' COMMENT 'poster email',
  `description` text COMMENT 'extra info',
  `care_level` enum('1','2','3','4','5') NOT NULL DEFAULT '3',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`ad_id`),
  KEY `user_fk` (`user`),
  CONSTRAINT `user_fk` FOREIGN KEY (`user`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

#
# Structure for table "equipment"
#

DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment` (
  `ad_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `type` enum('Tank','Cage','Leash','Food','Toy','Other') NOT NULL DEFAULT 'Tank' COMMENT 'enumerated type',
  `details` text COMMENT 'extra details',
  PRIMARY KEY (`ad_id`,`id`),
  CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`ad_id`) REFERENCES `advertisment` (`ad_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for table "pet"
#

DROP TABLE IF EXISTS `pet`;
CREATE TABLE `pet` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'pet name',
  `pic` varchar(255) DEFAULT NULL COMMENT 'img url',
  `colour` enum('Black','Brown','Red','White','Grey','Orange','Blue','Striped','Spotted','Other','Green') NOT NULL DEFAULT 'Black' COMMENT 'main pet colour',
  `b_date` date DEFAULT NULL COMMENT 'pet birthdate',
  `sex` enum('M','F') NOT NULL DEFAULT 'M' COMMENT 'pet sex',
  `species` enum('Dog','Cat','Snake','Lizard','Fish','Rabbit','Rodent','Frog','Turtle','Other') NOT NULL DEFAULT 'Dog',
  `friendliness` enum('1','2','3','4','5') NOT NULL DEFAULT '3',
  `ad_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_id` (`ad_id`),
  CONSTRAINT `ad_id_fk` FOREIGN KEY (`ad_id`) REFERENCES `advertisment` (`ad_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT 'user email',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT 'password',
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='super class for person and organization';

#
# Structure for table "sat_for"
#

DROP TABLE IF EXISTS `sat_for`;
CREATE TABLE `sat_for` (
  `user` varchar(255) NOT NULL DEFAULT '',
  `pet_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `times` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user`,`pet_id`),
  KEY `pet_id` (`pet_id`),
  CONSTRAINT `sat_for_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sat_for_ibfk_2` FOREIGN KEY (`pet_id`) REFERENCES `pet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for table "review"
#

DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `reviewer` varchar(255) NOT NULL DEFAULT '' COMMENT 'user email',
  `reviewee` varchar(255) NOT NULL DEFAULT '' COMMENT 'user email',
  `rating` smallint(5) unsigned DEFAULT NULL,
  `comment` text COMMENT 'comment',
  PRIMARY KEY (`reviewer`,`reviewee`),
  KEY `reviewee_fk` (`reviewee`),
  KEY `reviewer_fk` (`reviewer`),
  CONSTRAINT `reviewee_fk` FOREIGN KEY (`reviewee`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reviewer_fk` FOREIGN KEY (`reviewer`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for table "person"
#

DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `user` varchar(255) NOT NULL DEFAULT '',
  `fname` varchar(255) NOT NULL DEFAULT '',
  `mname` varchar(255) DEFAULT '',
  `lname` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`user`),
  CONSTRAINT `user_email_fk` FOREIGN KEY (`user`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='individual';

#
# Structure for table "feedback"
#

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `sitter` varchar(255) NOT NULL DEFAULT '',
  `pet_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `rating` smallint(5) DEFAULT '3',
  `comment` text,
  PRIMARY KEY (`sitter`,`pet_id`),
  KEY `feedback_ibfk_1` (`pet_id`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`pet_id`) REFERENCES `pet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`sitter`) REFERENCES `person` (`user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for table "owns"
#

DROP TABLE IF EXISTS `owns`;
CREATE TABLE `owns` (
  `user` varchar(255) NOT NULL DEFAULT '',
  `pet_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `adoption_date` date DEFAULT NULL,
  PRIMARY KEY (`user`,`pet_id`),
  KEY `pet_id` (`pet_id`),
  CONSTRAINT `owns_ibfk_1` FOREIGN KEY (`user`) REFERENCES `person` (`user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `owns_ibfk_2` FOREIGN KEY (`pet_id`) REFERENCES `pet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for table "organization"
#

DROP TABLE IF EXISTS `organization`;
CREATE TABLE `organization` (
  `user` varchar(255) NOT NULL DEFAULT '' COMMENT 'reference to user',
  `org_name` varchar(255) NOT NULL DEFAULT '',
  `for_profit` bit(1) NOT NULL DEFAULT b'1' COMMENT '0 = not for profit, 1 = for profit',
  PRIMARY KEY (`user`),
  CONSTRAINT `user` FOREIGN KEY (`user`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='organization table';

#
# Structure for table "cares_for"
#

DROP TABLE IF EXISTS `cares_for`;
CREATE TABLE `cares_for` (
  `org` varchar(255) NOT NULL DEFAULT '',
  `pet_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `source` enum('Street','Abuse','Unwanted','Other') NOT NULL DEFAULT 'Street',
  PRIMARY KEY (`org`,`pet_id`),
  KEY `pet` (`pet_id`),
  CONSTRAINT `cares_for_ibfk_1` FOREIGN KEY (`org`) REFERENCES `organization` (`user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cares_for_ibfk_2` FOREIGN KEY (`pet_id`) REFERENCES `pet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for table "interested_in"
#

DROP TABLE IF EXISTS `interested_in`;
CREATE TABLE `interested_in` (
  `user` varchar(255) NOT NULL DEFAULT '',
  `ad_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user`,`ad_id`),
  KEY `interested_in_ibfk_2` (`ad_id`),
  CONSTRAINT `interested_in_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `interested_in_ibfk_2` FOREIGN KEY (`ad_id`) REFERENCES `advertisment` (`ad_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for table "address"
#

DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `user` varchar(255) NOT NULL DEFAULT '',
  `street_addr` varchar(255) NOT NULL DEFAULT '',
  `pnum` varchar(15) DEFAULT NULL,
  `zip` varchar(7) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `city` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user`),
  CONSTRAINT `address_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

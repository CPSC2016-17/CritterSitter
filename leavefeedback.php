<?php
include_once("Scripts/dbconnect.php");

session_start();

if(isset($_POST['submit-feedback']))
{
	$insertFeedback = "INSERT INTO feedback VALUES ('". $_SESSION['user'] ."', '". $_POST['pet_id'] ."', '". $_POST['rating'] ."', '". $connection->real_escape_string($_POST['comment']) ."');";
	
	$connection->query($insertFeedback);
	
	header("Location: myreviews.php");
	
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - Feedback</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/postad.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	
    <h2>Leave Feedback</h2>
	<hr />
    
    <div id="inputform">
    	<form name="feedback" action="" method="post">
        	<table cellpadding="5px">
            <tbody>
                <tr><td><label>Rating:</label></td><td><input type="number" value="3" min="1" max="5" required="required" name="rating" /></td></tr>
                <tr><td><label>Comments:</label></td><td><textarea name="comment" required="required" rows="5"></textarea></td></tr>
            </tbody>
            </table>
            <br />
            <input type="hidden" name="pet_id" value="<?php echo $_GET['id'] ?>"/>
            <input style="float:none" type="submit" name="submit-feedback" value="Leave Feedback" /> 
        </form>
    </div>
    
</div>
</body>
</html>
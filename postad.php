<?php
include_once("Scripts/dbconnect.php");

session_start();

if(isset($_POST['submit-post']))
{
	postAd();
}

function postAd()
{
	global $connection;
	
	//Technically /should/ be changed to only insert and proceed if the session user = pet's owner... but this isn't a security course right? ;) 
	$insertAd = "INSERT INTO advertisment VALUES(null, '". $_SESSION['user'] ."', '". $connection->real_escape_string($_POST['description']) ."', '". $_POST['care_level'] ."', '". $_POST['start_date'] ."', '". $_POST['end_date'] ."');";

	echo $insertAd. "\n";

	$connection->query($insertAd);
	
	$adID = mysqli_fetch_assoc($connection->query("SELECT ad_id FROM advertisment WHERE ad_id=LAST_INSERT_ID();"));
	
	echo $adID. "\n";
	
	$adID = $adID['ad_id'];
	
	$updatePet = "UPDATE pet SET ad_id='$adID' WHERE id='". $_POST['pet_id'] . "';";
	
	echo $updatePet. "\n";
	
	$connection->query($updatePet);
	
	$i = 0;
	while( isset($_POST['type_'.$i]))
	{
		$insertEquip = "INSERT INTO equipment VALUES('$adID','$i', '". $_POST['type_'.$i] ."', '". $connection->real_escape_string($_POST['details_'.$i]) ."');";
			
		$connection->query($insertEquip);
		
		$i++;
	}
	
	header("Location: mypets.php");
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - Post Ad</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/postad.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Scripts/util.js"></script>
<script type="text/javascript" src="Scripts/postad.js"></script>
</head>

<body id="body">

<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	
    <h2>Create Ad</h2>
	<hr />
    
    <div id="inputform">
    	<form name="petAd" action="" method="post">
        	<table cellpadding="5px">
            <tbody>
 				<tr><td><label>Care needed from:</label></td><td><input type="date" required="required" name="start_date" /></td></tr>
                <tr><td><label>Care needed to:</label></td><td><input type="date" required="required" name="end_date" /></td></tr>
                <tr><td><label>Level of care required:</label></td><td style="text-align:left"><input type="number" max="5" min="1" value="3" required="required" name="care_level" /></td></tr>
                <tr><td><label>Description:</label></td><td><textarea style="float:left" name="description" rows="5"></textarea></td></tr>
                <tr><td><label>Equipment Needed:</label></td><td>
                	
                    <table id="equipment_table" border="1" width="100%">
                    	<tr><td colspan="2"><input style="display:inline-block" type="button" value="Add" onclick="addEquipment()"></td></tr>
                        <tr><th width="20%">Type</th><th width="auto">Details</th></tr>
                    </table>
                </td></tr>
            </tbody>
            </table>
            <br />
            <input type="hidden" name="pet_id" value="<?php echo $_GET['id'] ?>"/>
            <input style="float:none" type="submit" name="submit-post" value="Post Ad" /> 
        </form>
    </div>
    
</div>
</body>
</html>
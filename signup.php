<?php
include_once("Scripts/dbconnect.php");
include_once("Scripts/login.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - Sign Up</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/modal.css" rel="stylesheet" type="text/css">
<link href="Styles/signup.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php if(!isset($_SESSION['user'])){include_once("loginmodal.php"); loadLoginModal(false);} ?>
<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	<h2>Sign Up</h2>
    <hr />
    <div id="userTypeForm">
        <h3>I am an:</h3>
        <div id="userType">
		 	<a href="signup-ind.php<?php if(isset($_GET['email'])) echo('?email=' . $_GET['email']) ?>" onmouseover="this.style.cursor='pointer'"><img src="Images/ind.png" width="252" height="252" /></a>
            <a href="signup-org.php<?php if(isset($_GET['email'])) echo('?email=' . $_GET['email']) ?>" onmouseover="this.style.cursor='pointer'"><img src="Images/org.png" width="252" height="252" /></a>
        </div>
    </div>
    
</div>

</body>
</html>
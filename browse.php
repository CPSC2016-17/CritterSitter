<?php
include_once("Scripts/dbconnect.php");

date_default_timezone_set('UTC');

$speciesFlags = $_GET['species'];
$sexFlags = $_GET['sex'];
$locFlags = $_GET['loc'];
$friFlags = $_GET['fri'];
$startDate = $_GET['start'];
$endDate = $_GET['end'];
$posterFlags = $_GET['poster'];

$filter;

$sql = "SELECT * FROM advertisment AS ad, pet WHERE ad.ad_id = pet.ad_id";

if(!empty($speciesFlags))
{
	$filter = $filter . " AND (";
	
	for($i = 0; $i < count($speciesFlags); $i++)
	{	
		$filter = $filter .  "pet.species='".$speciesFlags[$i]."'";
		
		if($i < (count($speciesFlags) - 1))
		{
			$filter = $filter . " OR ";
		}
	}
	
	$filter = $filter . ")";
}

if(!empty($sexFlags))
{
	$filter = $filter . " AND (";
	
	for($i = 0; $i < count($sexFlags); $i++)
	{
		$filter = $filter . "pet.sex='".$sexFlags[$i]."'";
		
		if($i < (count($sexFlags)) - 1)
		{
			$filter = $filter . " OR ";
		}
	}
	
	$filter = $filter . ")";
}

if(!empty($locFlags))
{
	$filter = $filter . " AND (";
	
	for($i = 0; $i < count($locFlags); $i++)
	{
		$filter = $filter . "ad.care_level='".$locFlags[$i]."'";
		
		if($i < (count($locFlags)) - 1)
		{
			$filter = $filter . " OR ";
		}
	}
	
	$filter = $filter . ")";
}

if(!empty($friFlags))
{
	$filter = $filter . " AND (";
	
	for($i = 0; $i < count($friFlags); $i++)
	{	
		$filter = $filter .  "pet.friendliness='".$friFlags[$i]."'";
		
		if($i < (count($friFlags) - 1))
		{
			$filter = $filter . " OR ";
		}
	}
	
	$filter = $filter . ")";
}

if(!empty($posterFlags))
{
	if(in_array("ind", $posterFlags) and !in_array("org", $posterFlags))
	{
		$filter = $filter . " AND (ad.user IN (SELECT user FROM person))";
	}
	if(!in_array("ind", $posterFlags) and in_array("org", $posterFlags))
	{
		$filter = $filter . " AND (ad.user IN (SELECT user FROM organization))";
	}
}

if(!empty($startDate))
{
	$filter = $filter . " AND (ad.start_date >='". $startDate ."')";
}

if(!empty($endDate))
{
	$filter = $filter . " AND (ad.end_date <='". $endDate ."')";
}

$sql = $sql . $filter;

?>

<html>
<link href="Styles/browse.css" rel="stylesheet" type="text/css" />
<script src="Scripts/filter.js" type="text/javascript"></script>
<body>

<div class="filter">

	<h3>Filter Ads:</h3><hr>
    <form name="filter" method="get" action="">
        <div class="column">
            <h4 id="speciesHeader" class="dropdownHeader" onClick="openDropdown('species', 'speciesHeader')">Species ◄</h4>
            <div id="species" class="dropdown">
                <input type="checkbox" name="species[]" value="Dog" <?php echo (empty($_GET) || in_array( "Dog", $speciesFlags)) ? 'checked' : ''; ?>/><label>Dog</label><br>
                <input type="checkbox" name="species[]" value="Cat" <?php echo (empty($_GET) || in_array( "Cat", $speciesFlags)) ? 'checked' : ''; ?>/><label>Cat</label><br>
                <input type="checkbox" name="species[]" value="Snake" <?php echo (empty($_GET) || in_array( "Snake", $speciesFlags)) ? 'checked' : ''; ?>/><label>Snake</label><br>
                <input type="checkbox" name="species[]" value="Lizard" <?php echo (empty($_GET) || in_array( "Lizard", $speciesFlags)) ? 'checked' : ''; ?>/><label>Lizard</label><br>
                <input type="checkbox" name="species[]" value="Fish" <?php echo (empty($_GET) || in_array( "Fish", $speciesFlags)) ? 'checked' : ''; ?>/><label>Fish</label><br>
                <input type="checkbox" name="species[]" value="Rabbit" <?php echo (empty($_GET) || in_array( "Rabbit", $speciesFlags)) ? 'checked' : ''; ?>/><label>Rabbit</label><br>
                <input type="checkbox" name="species[]" value="Rodent" <?php echo (empty($_GET) || in_array( "Rodent", $speciesFlags)) ? 'checked' : ''; ?>/><label>Rodent</label><br>
                <input type="checkbox" name="species[]" value="Frog" <?php echo (empty($_GET) || in_array( "Frog", $speciesFlags)) ? 'checked' : ''; ?>/><label>Frog</label><br>
                <input type="checkbox" name="species[]" value="Turtle" <?php echo (empty($_GET) || in_array( "Turtle", $speciesFlags)) ? 'checked' : ''; ?>/><label>Turtle</label><br>
                <input type="checkbox" name="species[]" value="Other" <?php echo (empty($_GET) || in_array( "Other", $speciesFlags)) ? 'checked' : ''; ?>/><label>Other</label>
            </div>
        </div>
        <div class="column">
        	<h4 id="genderHeader" class="dropdownHeader" onClick="openDropdown('genders', 'genderHeader')">Gender ◄</h4>
            <div id="genders" class="dropdown">
            	<input type="checkbox" name="sex[]" value="M" <?php echo (empty($_GET) || in_array( "M", $sexFlags)) ? 'checked' : ''; ?>/><label>Male</label><br>
				<input type="checkbox" name="sex[]" value="F" <?php echo (empty($_GET) || in_array( "F", $sexFlags)) ? 'checked' : ''; ?>/><label>Female</label>
            </div>
        </div>
        <div class="column">
        	<h4 id="locHeader" class="dropdownHeader" onClick="openDropdown('levelsOfCare', 'locHeader')">Level of Care ◄</h4>
            <div id="levelsOfCare" class="dropdown">
            	<input type="checkbox" name="loc[]" value="1" <?php echo (empty($_GET) || in_array( "1", $locFlags)) ? 'checked' : ''; ?>/><label>1 - Minimal</label><br>
				<input type="checkbox" name="loc[]" value="2" <?php echo (empty($_GET) || in_array( "2", $locFlags)) ? 'checked' : ''; ?>/><label>2</label><br>
				<input type="checkbox" name="loc[]" value="3" <?php echo (empty($_GET) || in_array( "3", $locFlags)) ? 'checked' : ''; ?>/><label>3 - Average</label><br>
                <input type="checkbox" name="loc[]" value="4" <?php echo (empty($_GET) || in_array( "4", $locFlags)) ? 'checked' : ''; ?>/><label>4</label><br>
                <input type="checkbox" name="loc[]" value="5" <?php echo (empty($_GET) || in_array( "5", $locFlags)) ? 'checked' : ''; ?>/><label>5 - Lots</label>
            </div>
        </div>
        <div class="column">
        	<h4 id="friHeader" class="dropdownHeader" onClick="openDropdown('friendliness', 'friHeader')">Friendliness ◄</h4>
            <div id="friendliness" class="dropdown">
            	<input type="checkbox" name="fri[]" value="1" <?php echo (empty($_GET) || in_array( "1", $friFlags)) ? 'checked' : ''; ?>/><label>1 - Hostile</label><br>
				<input type="checkbox" name="fri[]" value="2" <?php echo (empty($_GET) || in_array( "2", $friFlags)) ? 'checked' : ''; ?>/><label>2</label><br>
				<input type="checkbox" name="fri[]" value="3" <?php echo (empty($_GET) || in_array( "3", $friFlags)) ? 'checked' : ''; ?>/><label>3 - Average</label><br>
                <input type="checkbox" name="fri[]" value="4" <?php echo (empty($_GET) || in_array( "4", $friFlags)) ? 'checked' : ''; ?>/><label>4</label><br>
                <input type="checkbox" name="fri[]" value="5" <?php echo (empty($_GET) || in_array( "5", $friFlags)) ? 'checked' : ''; ?>/><label>5 - Very</label>
            </div>
        </div>
        <div class="column">
        	<h4 id="dateHeader" class="dropdownHeader" onClick="openDropdown('dates', 'dateHeader')">Availability ◄</h4>
            <div id="dates" class="dropdown">
            	<label>Available From</label><br>
                <input type="date" name="start" value="<?php echo (isset($startDate)) ? $startDate : ''; ?>"/><br>
				<label>Available To</label><br>
                <input type="date" name="end" value="<?php echo (isset($endDate)) ? $endDate : '' ?>"/>
            </div>
        </div>
        <div class="column">
        	<h4 id="posterHeader" class="dropdownHeader" onClick="openDropdown('posters', 'posterHeader')">Ads Posted by ◄</h4>
            <div id="posters" class="dropdown">
                <input type="checkbox" name="poster[]" value="ind" <?php echo (empty($_GET) || in_array( "ind", $posterFlags)) ? 'checked' : ''; ?>/><label>Individuals</label><br>
                <input type="checkbox" name="poster[]" value="org" <?php echo (empty($_GET) || in_array( "org", $posterFlags)) ? 'checked' : ''; ?>/><label>Organizations</label>
            </div>
        </div>
        <input class="applyButton" type="submit" value="Apply"/>
    </form>
</div>
</body>
</html>
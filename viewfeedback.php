<?php
include_once("Scripts/dbconnect.php");

session_start();


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - Reviews & Feedback</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/myreviews.css" rel="stylesheet" type="text/css" />
<link href="Styles/sidebyside.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	<h2>Feedback</h2>
    <hr />
    
    <table width="60%" border="1" bordercolordark="#FFF" bordercolorlight="#FFF" cellpadding="5px">
    
    <?php
	
		$getAllFeedback = "SELECT fname, rating, comment FROM person, feedback WHERE user=sitter AND pet_id='". $_GET['id'] ."';";
		$result = $connection->query($getAllFeedback);
		
		while($feedback = mysqli_fetch_assoc($result))
		{
			
			echo '<tr><td>';
			
				echo '<h3 style="margin-bottom:0; text-align:left">'. $feedback['fname'] .' Says:</h3>';
				echo '<p style="text-align:left">'. $feedback['comment'] .'</p>';
			
			echo '</td><td width="10%">';
					
				echo '<h3 style="margin-bottom:0; font-size:20px">Rating:</h3><h4 style="margin-top:10px; margin-bottom:0px; font-size:20px">'. $feedback['rating'] .'</h4>';
			
			echo '</td></tr>';
		}
	
	?>
    
    </table>
    
</div>

</body>
</html>
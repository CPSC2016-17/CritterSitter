<?php
include_once("Scripts/dbconnect.php");
include_once("Scripts/login.php");
include_once("Scripts/signup-methods.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - Sign Up</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/modal.css" rel="stylesheet" type="text/css">
<link href="Styles/signup.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Scripts/util.js"></script>
<script type="text/javascript" src="Scripts/signupFormVerification.js"></script>
</head>

<body id="body">

<?php include_once("loginmodal.php"); loadLoginModal(false); ?>

<?php if(!isset($_SESSION['user'])){include_once("loginmodal.php"); loadLoginModal(false);} ?>
<?php include_once("Scripts/header.php"); loadHeader(false);?>


<div id="content">
	<h2>Sign Up</h2>
    <hr />
	<div id="inputform">
    	<form name="orgSignupForm" action="" method="post">
        	<table cellpadding="5px">
            <tbody>
        	<tr><td><label>Email:</label></td><td><input required="required" type="email" name="email" value="<?php echo($_GET['email']) ?>" /></td></tr>
            <tr><td><label>Password:</label></td><td><input required="required" id="pass" type="password" name="password" /></td></tr>
			<tr><td><label>Confirm Password:</label></td><td><input required="required" id="pass-conf" onchange="verify()" type="password" name="password-confirm"/></td></tr>
			<tr><td><label>Organization Name:</label></td><td><input required="required" type="text" name="org_name" /></td></tr>
            <tr><td><label>Organization Street Address:</label></td><td><input required="required" type="text" name="street_addr" /></td></tr>
            <tr><td><label>City:</label></td><td><input required="required" type="text" name="city" /></td></tr>
            <tr><td><label>Country:</label></td><td><input required="required" type="text" name="country" /></td></tr>
            <tr><td><label>Organization Postal Code:</label></td><td><input required="required" type="text" name="zip" /></td></tr>
            <tr><td><label>Organization Phone Number:</label></td><td><input required="required" type="tel" name="pnum" /></td></tr>
			<tr><td><label>For-profit:</label></td><td class="checkbox"><input name="for_profit" type="checkbox" /></td></tr>
            </tbody>
            </table>
            <br />
            <input type="submit" name="signup-org" value="Sign Up" /> 
        </form>
    </div>
    
</div>

</body>
</html>
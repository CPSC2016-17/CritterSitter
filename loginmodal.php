<?php

include_once("Scripts/login.php");

function loadLoginModal($showSignup = true)
{
	echo('<div id="loginModal" class="modal">
	<div class="modalContent">
    	<div class="modalHeader">
        	<span class="close" onclick="hideLoginModal()" onmouseover="this.style.cursor=\'pointer\'">&times;</span>
        </div>
        <div class="modalBody">
    		<form name="login" action="" method="post">
            	Email: <br>
                <input type="email" name="email"> <br>
                Password:<br>
                <input type="password" name="password"><br><br>
                <input type="submit" name="submit-login" value="Login">');
				
	if($showSignup)
	{
		echo(' or 
                <input type="submit" name="submit-signup" value="Sign Up">');
	}
	
	echo('</form>
   		</div>
    </div>
</div>');
}

?>

<?php

include_once("Scripts/dbconnect.php");

session_start();

if(isset($_POST['accept_sitter']))
{
	
	$getPetID = "SELECT id FROM pet WHERE ad_id='". $_POST['ad_id'] ."';";
	
	$petidresult = $connection->query($getPetID);
	
	$petid = mysqli_fetch_assoc($petidresult);
	
	//Add to sat_for
	$insertSatFor = "INSERT INTO sat_for ( user, pet_id, times ) VALUES('". $_POST['sitter'] ."', '". $petid['id'] ."', '1') ON DUPLICATE KEY UPDATE times=times+1;";
	
	$connection->query($insertSatFor);
	
	//delete ad
	$deleteAd = "DELETE FROM advertisment WHERE ad_id='".$_POST['ad_id']."' AND user='". $_SESSION['user'] ."';";
	
	$connection->query($deleteAd);
	
	//go to mypets 
	$_SESSION['sitter_found'] = $_POST['sitter'];
	header("Location: mypets.php");
	
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - Interested Sitters</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/petresults.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	<h2>Interested Sitters</h2>
    <hr />
    
    <table id="sitters" width="80%" border="1" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF">
    <th width="20%">Name</th><th width="60%">Reviews</th><th width="10%">Avg. Rating</th><th width="10%">Actions</th>
    
    <?php
	
		$usersInterestedInAd = "SELECT user FROM interested_in WHERE ad_id='". $_GET['ad_id'] ."';";
	
		//echo $usersInterestedInAd;
		
		$results = $connection->query($usersInterestedInAd);
		
		//var_dump($results);
		
		while($row = mysqli_fetch_assoc($results))
		{
			$getInterestedUseresNames = "SELECT fname, lname FROM person WHERE user='". $row['user'] ."';";
			
			$getReviews = "(SELECT fname AS reviewer, comment FROM review, person WHERE reviewee='".$row['user']."' AND user = reviewer) UNION (SELECT org_name AS reviewer, comment FROM review, organization WHERE reviewee='". $row['user'] ."' AND user = reviewer);";
			
			$getAvgRating = "SELECT AVG(rating) AS avg_rating FROM review WHERE reviewee='". $row['user'] ."';";
			
			$names = $connection->query($getInterestedUseresNames);
			$reviews = $connection->query($getReviews);
			$avgRating = $connection->query($getAvgRating);
			
			//var_dump($names);
			
			$names = mysqli_fetch_assoc($names);
			$avgRating = mysqli_fetch_assoc($avgRating);
			
			//var_dump($entry);
			
			echo '<tr><td>'. $names['fname'] .', '. $names['lname'] . '</td>';
				echo'<td>';
					
					echo '<table width="100%" border="1" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF">';
					echo '<th width="80%">Comments</th><th width="20%">Reviewer</th>';	
						while($review = mysqli_fetch_assoc($reviews))
						{
							echo '<tr>';
								echo '<td width="80%">'. $review['comment'] .'</td>';
								echo '<td width="20%">'. $review['reviewer'] . '</td>';
							echo '</tr>';
						}
					
					echo '</table>';
				echo '</td>';
				
				if(isset($avgRating['avg_rating']))
				{
					echo '<td>'. round($avgRating['avg_rating'], 1) .'</td>';
				}
				else
				{
					echo '<td>N/A</td>';
				}
				echo '<td><form method="post" action=""><input type="submit" name="accept_sitter" value="Accept"/><input type="hidden" name="ad_id" value="'. $_GET['ad_id'] .'"/><input type="hidden" name="sitter" value="'. $row['user'] .'"/></form></tr>';
			
			
		}
		
		
	
	?>
    
	
    </table>
    
    
</div>

</body>
</html>
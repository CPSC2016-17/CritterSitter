<?php
include_once("Scripts/dbconnect.php");

session_start();


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Critter Sitter - Reviews & Feedback</title>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
<link href="Styles/header.css" rel="stylesheet" type="text/css">
<link href="Styles/page.css" rel="stylesheet" type="text/css">
<link href="Styles/myreviews.css" rel="stylesheet" type="text/css" />
<link href="Styles/sidebyside.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Scripts/util.js"></script>
</head>

<body id="body">

<?php include_once("Scripts/header.php"); loadHeader(false);?>

<div id="content">
	<h2>Feedback & Reviews</h2>
    <hr />
    
    <?php 
		if($_SESSION['type'] == "ind")
		{
			echo "<p>Leave feedback on pets you've sat for, or leave a review for someone who has sat for you.</p>";
			
			echo '<div class="container">';
			
			echo '<div class="fixed">';
			
				echo '<table width="100%" border="1" bordercolordark="#FFF" bordercolorlight="#FFF" >';
				echo '<th>Pets I\'ve Watched</th>';
				
				$getPetsIveWatched = "SELECT DISTINCT id, name, pic FROM pet WHERE id IN (SELECT pet_id FROM sat_for WHERE user='". $_SESSION['user'] ."');";
				$result = $connection->query($getPetsIveWatched);
				
				while($pet = mysqli_fetch_assoc($result))
				{
					echo '<tr><td><table><tr>';
						echo '<td width="50%"><img width="100%" height="auto" src="'. $pet['pic'] .'"/></td>';
						
						$checkIfFeedbackExists = "SELECT pet_id FROM feedback WHERE sitter='". $_SESSION['user'] ."' AND pet_id='". $pet['id'] ."';";
						$fbe = $connection->query($checkIfFeedbackExists);
												
						$buttonText = "Leave Feedback";
						$enabled = TRUE;
						if($fbe->num_rows > 0)
						{
							$buttonText = "Feedback Left";
							$enabled = FALSE;
						}
						
						echo '<td><h3>'. $pet['name'] . '</h3><br/><button onclick="location.href=\'leavefeedback.php?id='. $pet['id'] .'\'" '. ($enabled ? ''  :'disabled') .' >'.$buttonText.'</button></td>';
					echo '</tr></table></td></tr>';
				}
				
				echo '</table>';
			echo '</div>';
			echo '<div class="fixed">';
			
				echo '<table width="100%" border="1" bordercolordark="#FFF" bordercolorlight="#FFF" >';
				echo '<th>Sitters I\'ve Used</th>';
				
				$getSitters = "SELECT DISTINCT user, fname, lname FROM person WHERE user IN (SELECT user FROM sat_for WHERE pet_id IN (SELECT pet_id FROM owns WHERE user='". $_SESSION['user'] ."'));";
				$result = $connection->query($getSitters);
				
				while($sitter = mysqli_fetch_assoc($result))
				{
					echo '<tr><td><table width="100%"><tr>';
						echo '<td width="50%"><h3 style="margin-bottom:auto">'. $sitter['fname'] .', '. $sitter['lname'] .'</h3></td>';
						
						$checkIfReviewExists = "SELECT reviewer FROM review WHERE reviewer='". $_SESSION['user'] ."' AND reviewee='". $sitter['user'] ."';";
						$re = $connection->query($checkIfReviewExists);
												
						$buttonText = "Give a Review";
						$enabled = TRUE;
						
						if($re->num_rows > 0)
						{
							$buttonText = "Review Given";
							$enabled = FALSE;
						}
						
						
						echo '<td><button onclick="location.href=\'givereview.php?sitter='. $sitter['user'] .'\'" '. ($enabled ? '' : 'disabled') .'>'. $buttonText .'</button></td>';
					
					echo '</tr></table></td></tr>';
				}
				echo '</table>'; 
			
			echo '</div>';
		}
		else
		{
			echo "<p>Leave a review for a sitter who has watched your pet.</p>";
			
			echo '<div style="width:50%; margin: 10 auto;">';
			
				echo '<table width="100%" border="1" bordercolordark="#FFF" bordercolorlight="#FFF" >';
				echo '<th>Sitters I\'ve Used</th>';
				
				$getSitters = "SELECT DISTINCT user, fname, lname FROM person WHERE user IN (SELECT user FROM sat_for WHERE pet_id IN (SELECT pet_id FROM cares_for WHERE org='". $_SESSION['user'] ."'));";
				$result = $connection->query($getSitters);
				
				while($sitter = mysqli_fetch_assoc($result))
				{
					echo '<tr><td><table width="100%"><tr>';
						echo '<td width="50%"><h3 style="margin-bottom:auto">'. $sitter['fname'] .', '. $sitter['lname'] .'</h3></td>';
						
						$checkIfReviewExists = "SELECT reviewer FROM review WHERE reviewer='". $_SESSION['user'] ."' AND reviewee='". $sitter['user'] ."';";
						$re = $connection->query($checkIfReviewExists);
												
						$buttonText = "Give a Review";
						$enabled = TRUE;
						
						if($re->num_rows > 0)
						{
							$buttonText = "Review Given";
							$enabled = FALSE;
						}
						
						
						echo '<td><button onclick="location.href=\'givereview.php?sitter='. $sitter['user'] .'\'" '. ($enabled ? '' : 'disabled') .'>'. $buttonText .'</button></td>';
					
					echo '</tr></table></td></tr>';
				}
				echo '</table>'; 
		}
	?>
    </div>
    
    <div>
    
    <?php 
		
		if($_SESSION['type'] == "ind")
		{
			echo '<h2>My Sitting Reviews</h2>';
			
			$getAvgRating = "SELECT AVG(rating) AS avg FROM review WHERE reviewee='". $_SESSION['user'] ."';";
			$result = $connection->query($getAvgRating);
			$avgRating = mysqli_fetch_assoc($result);
			
			if(empty($avgRating['avg']))
			{
				echo 'Average Rating: N/A';	
			}
			else
			{
				echo 'Average Rating: '. round($avgRating['avg'], 1) .'<hr/>';
			}
			echo '<table width="60%" border="1" bordercolordark="#FFF" bordercolorlight="#FFF" cellpadding="5px">';
				
				$getAllReviews = "(SELECT p.fname AS author, r.rating, r.comment FROM review AS r, person AS p WHERE r.reviewee='". $_SESSION['user'] ."' AND p.user=r.reviewer) 
									UNION 
								  (SELECT o.org_name AS author, r.rating, r.comment FROM organization AS o, review AS r WHERE r.reviewee='". $_SESSION['user'] ."' AND o.user=r.reviewer);";
				$result = $connection->query($getAllReviews);
				
				while($review = mysqli_fetch_assoc($result))
				{
					echo '<tr><td>';
					
						echo '<h3 style="margin-bottom:0; text-align:left">'. $review['author'] .' Says:</h3>';
						echo '<p style="text-align:left">'. $review['comment'] .'</p>';
					
					echo '</td><td width="10%">';
					
						echo '<h3 style="margin-bottom:0; font-size:20px">Rating:</h3><h4 style="margin-top:10px; margin-bottom:0px; font-size:20px">'. $review['rating'] .'</h4>';
					
					echo '</td></tr>';
				}
			
			echo '</table>';
		}
		
	?>
    
    </div>
    
    
    
    
</div>

</body>
</html>